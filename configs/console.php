<?php

$config = [
    'id'                  => 'yii2-gii',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [],
    'controllerNamespace' => 'app\commands',
    'aliases'             => [],
    'components'          => [],
    'params'              => [],
];

return $config;