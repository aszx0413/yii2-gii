# Yii2-gii but the best one

Let's get started.

# Features

- 对处理 Excel 中的空格，因为有太多字符会导致空格，所以要求在使用前必须保证文件内容的正确性。
- Excel 最后一个表的后面一定要带有`END`标识，否则会导致最后一个表丢失。
- [-] 列表搜索项配置
- [-] 配置项应该放哪？Model 还是 params

➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️

## AR

AR 基础类作为项目 models 的基类

### 变量

- const STATUS$

### 方法

- beforeSave

model 调用 save() 方法前的逻辑处理。

- findOrNew

根据 ID 找一个对象，框架默认写法：
```php
// 如果不存在则返回 null
$admin = Admin::findOne(0);
```
所以如果需要不存在时返回空对象的写法：
```php
$admin = Admin::findOne(0) ?? new Admin();

// 如果封装 findOrNew
$admin = Admin::findOrNew(0);
```

➡️➡️➡️➡️➡️➡️➡️➡️➡️➡️

## _Controller

全局 HTTP 请求的基础 Ctrl。


### 常量

#### URL 相关

- /home/index

RTD=''
RTC='home'
RTM='index'

- /member/home/index

RTD='member'
RTC='home'
RTM='index'

- /member/module1/ctrl/action

RTD='module1'
RTC='ctrl'
RTM='action'

## CFG

### Mineadmin

- H - hide 是否显示

- E - Enum TABLE_COL

- S - search

# Usage

## Excel

Excel 格式如下：
//qn1.10soo.net/assets/20221020112457.png

## 运行

```bash
# 生成所有
./yii mygii/all

./yii mygii/district --t
```