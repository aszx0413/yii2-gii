<?php
namespace aszx0413\yii2gii;

use yii\helpers\Inflector;

class Many
{
    /**
     * @var string 字段名
     */
    public $name = '';

    /**
     * @var string 所属表名
     *             为什么不用 table 对象？因为这时候关联的 table 对象可能还没生成
     */
    public $belongTableName = '';

    /**
     * @var Table 关联表
     */
    public $relateTable;

    public function __construct($on)
    {
        $this->name = $on;
    }

    public function asClass()
    {
        return $this->relateTable->class;
    }

    public function asProperty()
    {
        if ($this->name == 'parent_id') {
            return 'children';
        } else {
            $prefix = str_replace($this->belongTableName . '_id', '', $this->name);

            return lcfirst(Inflector::camelize($prefix . $this->relateTable->class)) . 'List';
        }
    }
}