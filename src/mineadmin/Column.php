<?php

namespace aszx0413\yii2gii\mineadmin;

use aszx0413\yii2gii\Field;
use aszx0413\yii2gii\Table;
use Yii;

class Column
{

	public static function generate(Table $tb)
	{
		// import语句
		$ctImport = 'import { STATUS';

		$ct = 'export default [' . "\n";

		$ct .= "\t" . '{
        dataIndex: "id",
        title: "ID",
        addDisplay: false,
        editDisplay: false,
        hide: false,
        align: "center",
    },' . "\n";

		foreach ($tb->fields as $field) {
			$tmp      = self::single($field);
			$ctImport .= $tmp['import'];
			$ct       .= $tmp['ct'];
		}

		// status
		$ct .= "\t" . '{
        dataIndex: "status",
        title: "状态",
        formType: "radio",
        dict: {
            data: STATUS,
            translation: true,
        },
        addDisplay: true,
        editDisplay: true,
        search: true,
        addDefaultValue: 1,
    },
';
		$ct .= ']';

		$ctImport .= ' } from "../params";' . "\n\n";

		$ct = $ctImport . $ct;

		file_put_contents(Yii::$app->basePath . '/runtime/columns/' . $tb->name . '.js', $ct);
	}

	private static function single(Field $field)
	{
		$import = '';

		$ct = "\t" . '{
        dataIndex: "' . $field->name . '",
        title: "' . $field->asLabel() . '",
        addDisplay: true,
        editDisplay: true,
        align: "center",' . "\n\n";

		// ------------------------------
		// hide
		// ------------------------------
		if ($field->type == 'TEXT') {
			$ct .= "\t\t" . 'hide: true,' . "\n";
		} elseif ($field->length >= 100) {
			$ct .= "\t\t" . 'hide: true,' . "\n";
		} elseif ($field->cfg['hide'] == 'true') {
			$ct .= "\t\t" . 'hide: true,' . "\n";
		} else {
			$ct .= "\t\t" . 'hide: false,' . "\n";
		}

		// ------------------------------
		// formType
		// ------------------------------
		if (str_ends_with($field->name, '_date') || $field->name == 'date') {
			// ------------------------------
			// 日期
			// ------------------------------
			$ct .= "\t\t" . 'formType: "date",' . "\n";
		} elseif (str_starts_with($field->nameCn, '@')) {
			// ------------------------------
			// 外键
			// ------------------------------
			$ct .= "\t\t" . 'formType: "select",' . "\n";
			$ct .= "\t\t" . 'dict: {' . "\n";
			$ct .= "\t\t\t" . 'url: "' . $field->asProperty() . '/listAll",' . "\n";
			$ct .= "\t\t\t" . 'props: { label: "' . ($field->comment ?: 'name') . '", value: "id" },' . "\n";
			$ct .= "\t\t\t" . 'translation: true,' . "\n";
			$ct .= "\t\t" . '},' . "\n";
		} elseif ($field->cfg['enum']) {
			// ------------------------------
			// 枚举
			// ------------------------------
			$var = strtoupper($field->table->name . '_' . $field->name);

			$ct .= "\t\t" . 'formType: ' . $var . '.length > 3 ? "select" : "radio",' . "\n";
			$ct .= "\t\t" . 'dict: {' . "\n";
			$ct .= "\t\t\t" . 'data: ' . $var . ',' . "\n";
			$ct .= "\t\t\t" . 'translation: true,' . "\n";
			$ct .= "\t\t" . '},' . "\n";

			$import .= ', ' . $var;
		} elseif ($field->length >= 100) {
			// ------------------------------
			// textarea
			// ------------------------------
			$ct .= "\t\t" . 'formType: "textarea",' . "\n";
		}

		// ------------------------------
		// 表单配置
		// ------------------------------
		$ct .= "\t\t" . 'addDisabled: ' . $field->cfg['addDisabled'] . ',' . "\n";
		$ct .= "\t\t" . 'editDisabled: ' . $field->cfg['editDisabled'] . ',' . "\n";

		// ------------------------------
		// commonRules
		// ------------------------------
		$commonRules = "";
		if ($field->cfg['required'] == 'true') {
			$commonRules .= 'required: true,';
		}
		if (str_contains($field->type, 'INT')) {
			$commonRules .= 'type: "number", min: 0,';
		}
		if ($commonRules) {
			$ct .= "\t\t" . 'commonRules: [{ ' . $commonRules . ' message: "输入有误" }],' . "\n";
		}

		// ------------------------------
		// 搜索
		// ------------------------------
		$ct .= "\t\t" . 'search: ' . ($field->cfg['search'] ?: 'false') . ',' . "\n";

		// ------------------------------
		// 默认值
		// ------------------------------

		$ct .= "\t" . '},
';

		return [
			'import' => $import,
			'ct'     => $ct,
		];
	}
}