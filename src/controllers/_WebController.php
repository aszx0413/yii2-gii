<?php
namespace aszx0413\yii2gii\controllers;

class _WebController extends _Controller
{
    public function render($view, $params = [])
    {
        $html = '';

        // ---------- 视图 ----------

        if (is_array($view)) {
            $params = $view;
            $view   = '';
        }

        if (empty($view)) {
            if (RTM == 'add') {
                $view = 'edit.html';
            } else {
                $view = RTM . '.html';
            }
            $view = str_replace('-', '_', RTC) . '_' . $view;
        }

        // ---------- 目录 ----------

        $renderDir = '';
        if ($this->globals['renderDir']) {
            $renderDir .= '/' . $this->globals['renderDir'];
        }

        // ---------- 数据 ----------

        $data = array_merge($this->globals, $params);

        $html .= $this->render($renderDir . '/_head.html', $data);
        $html .= $this->render($renderDir . '/' . $view, $data);
        $html .= $this->render($renderDir . '/_foot.html', $data);

        return $html;
    }
}
