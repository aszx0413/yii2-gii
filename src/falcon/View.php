<?php
namespace aszx0413\yii2gii\falcon;

use aszx0413\yii2gii\Table;

class View
{
    /**
     * 基于 falcon 生成列表模板
     * @param Table $tb
     * @return void
     */
    public static function generateList(Table $tb)
    {
        // --------------------------------------------------
        // 搜索区域
        // --------------------------------------------------
        $html = '
<form action="" method="get">
    <div class="row mb-3">
        <div class="col">
            <div class="form-group d-flex align-items-center">';
        foreach ($tb->fields as $field) {
            if ($field->isSearch()) {
                if (str_contains($field->enum, 'E')) {
                    $html .= '
                    <select class="form-select d-inline w-auto me-2" name="' . $field->name . '">
                        <?=\app\libs\Html::optionsKV(\app\models\\' . $field->table->class . '::' . strtoupper(str_replace('_id',
                            '', $field->name)) . ', 0, $_GET[\'' . $field->name . '\'])?>
                    </select>';
                } else {
                    $html .= '
                    <input type="text" class="form-control d-inline w-auto me-2" ';
                    $html .= 'name="' . $field->name . '" ';
                    $html .= 'value="<?=$_GET[\'' . $field->name . '\']?>" ';
                    $html .= 'placeholder="' . $field->nameCn . '" />';
                }
            }
        }
        $html .= '
                <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> 搜索</button>
            </div>
        </div>
        <div class="col-3 d-flex justify-content-end">
            <a href="/adm/<?=RTC?>/edit" class="btn btn-primary float-right ml-2" role="button" aria-pressed="true"><i class="fa fa-plus"></i> 新增</a>
        </div>
    </div>
</form>

';

        // $trTitle 列表表头
        $trTitle = '<th>ID</th>' . "\n";

        // $trTitle 列表数据
        $trBody = '<th><?=$v[\'id\']?></th>' . "\n";

        // 循环输出每个字段
        foreach ($tb->fields as $field) {
            if ($field->isList()) {
                $trTitle .= "\t\t\t\t\t" . '<th>' . $field->asLabel() . '</th>' . "\n";

                if ($field->isImg()) {
                    // --------------------------------------------------
                    // 显示图片
                    // --------------------------------------------------
                    $trBody .= "\t\t\t\t\t" . '<td><img src="<?=\app\libs\Html::image($v[\'' . $field->name . '\'],\'40x40\')?>" class="img-thumbnail"></td>' . "\n";
                } elseif (str_contains($field->enum, 'E')) {
                    // --------------------------------------------------
                    // 枚举。标识E
                    // --------------------------------------------------
                    $enumAttr = strtoupper(str_replace('_id', '', $field->name));
                    $trBody   .= "\t\t\t\t\t" . '<td><?=\app\models\\' . $field->table->class . '::' . $enumAttr . '[$v[\'' . $field->name . '\']]?></td>' . "\n";;
                } elseif (str_starts_with($field->nameCn, '@')) {
                    // --------------------------------------------------
                    // 关联属性
                    // --------------------------------------------------
                    $trBody .= "\t\t\t\t\t" . '<td><?=$v[\'' . $field->asProperty() . '\'][\'name\']?></td>' . "\n";
                } else {
                    // --------------------------------------------------
                    // 输出文本
                    // --------------------------------------------------
                    $trBody .= "\t\t\t\t\t" . '<td><?=$v[\'' . $field->name . '\']?></td>' . "\n";
                }
            }
        }

        $trTitle .= "\t\t\t\t\t" . '<th>新增时间</th>' . "\n";
        $trBody  .= "\t\t\t\t\t" . '<td><?=$v[\'created_at\']?></td>' . "\n";
        $trTitle .= "\t\t\t\t\t" . '<th>状态</th>' . "\n";
        $trBody  .= "\t\t\t\t\t" . '<td><?=$v->getStatusHtml()?></td>' . "\n";
        $trTitle .= "\t\t\t\t\t" . '<th class="text-right">操作</th>' . "\n";
        $trBody  .= "\t\t\t\t\t" . '<td class="text-right"><?=\\app\\libs\\Html::editDel(RTC,$v[\'id\'])?></td>' . "\n";

        $html .= '
<div class="row">
    <div class="col">
        <table class="table table-hover table-bordered bg-white">
            <thead class="thead-light">
                <tr>
                    ' . $trTitle . '
                </tr>
            </thead>
            <tbody>
                <?php foreach($list as $v):?>
                <tr>
                    ' . $trBody . '
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <nav>
            <?=$pagination->create()?>
        </nav>
    </div>
</div>';

        file_put_contents(\Yii::$app->basePath . '/views/adm/' . $tb->name . '_list.html', $html);
    }

    /**
     * 基于 falcon 生成表单模板
     *
     * @param Table $tb
     * @return void
     */
    public static function generateEdit(Table $tb)
    {
        if (!$tb->isForm) {
            return;
        }

        $html = '<div class="container">
    <div class="row card">
        <div class="col-12 card-body">
            <form class="form-validation" id="form1" method="post">
                <input type="hidden" name="id" value="<?=$row[\'id\']?>">
                ';

        foreach ($tb->fields as $field) {
            if (str_contains($field->form, 'G')) {
                // --------------------------------------------------
                // 多图片上传
                // --------------------------------------------------
                $colHtml = "<?=\app\libs\Html::uploadImg(\$row['" . $field->name . "'], '" . $field->name . "', true)?>";
            } elseif (str_starts_with($field->name, 'img_')) {
                // --------------------------------------------------
                // 如果是 img_ 开头的字段，表示是图片上传
                // --------------------------------------------------
                if (str_contains($field->form, 'G')) {
                    $multi = 'true';
                } else {
                    $multi = 'false';
                }
                $colHtml = "<?=\app\libs\Html::uploadImg(\$row['" . $field->name . "'], '" . $field->name . "', " . $multi . ")?>";
            } elseif (str_contains($field->enum, 'E')) {
                // --------------------------------------------------
                // 枚举。标识E
                // --------------------------------------------------
                $enumAttr = strtoupper(str_replace('_id', '', $field->name));
                $colHtml  = '
                    <div class="col-10">
                        <select class="form-select d-inline w-auto" name="' . $field->name . '">
                            <?=\app\libs\Html::optionsKV(\app\models\\' . $field->table->class . '::' . $enumAttr . ', 0, $row[\'' . $field->name . '\'])?>
                        </select>
                    </div>';
            } elseif (str_starts_with($field->nameCn, '@')) {
                // --------------------------------------------------
                // 下拉选择
                // --------------------------------------------------
                // $options[\'' . $field->asProperty() . 'List\']
                // \app\models\Cate::findAll(['status'=>1])
                $colHtml = '
                    <div class="col-10">
                        <select class="form-select d-inline w-auto" name="' . $field->name . '">
                            <?=\app\libs\Html::options($options[\'' . $field->hasOneTable->asProperty() . 'List\'] ?: \app\models\\' . $field->hasOneTable->class . '::findAll([\'status\'=>1]), 0, $row[\'' . $field->name . '\'], \'name\', \'id\')?>
                        </select>
                    </div>';
            } elseif (str_contains($field->form, 'TPL')) {
                // --------------------------------------------------
                // 使用模板
                // --------------------------------------------------
                $colHtml = '
                    <div class="col-10">
                        <?php include(\Yii::$app->basePath . \'/views/adm/' . $field->table->name . '_edit_' . $field->name . '.html\'); ?>
                    </div>';
            } elseif (str_contains($field->form, 'R')) {
                // --------------------------------------------------
                // 单选
                // --------------------------------------------------
                $colHtml = '
                    <div class="col-10 d-flex align-items-center">
                        <?=\app\libs\Html::radios(\'' . $field->name . '\', [0=>\'否\',1=>\'是\'],$row[\'' . $field->name . '\'])?>
                    </div>';
            } elseif ($field->type == 'TEXT' || ($field->length > 200)) {
                // --------------------------------------------------
                // 多行文本
                // --------------------------------------------------
                $colHtml = '
                    <div class="col-10">
                        <textarea class="form-control" name="' . $field->name . '" rows="3"><?=$row[\'' . $field->name . '\']?></textarea>
                    </div>';
            } else {
                // --------------------------------------------------
                // 输入框
                // --------------------------------------------------
                $col = 5;
                if ($field->length >= 50) {
                    $col = 10;
                }
                $colHtml = '
                    <div class="col-' . $col . '">
                        <input type="text" class="form-control" name="' . $field->name . '" value="<?=$row[\'' . $field->name . '\']?>" placeholder="">
                    </div>';
            }

            $html .= '
                <div class="form-group row mb-3">
                    <label class="col-1 col-form-label">' . $field->asLabel() . '</label>
                    ' . $colHtml . '
                </div>';
        }

        $html .= '
                <div class="form-group row mb-3">
                    <label class="col-1 col-form-label">状态</label>
                    <div class="col-10 d-flex align-items-center">
                        <?=\app\libs\Html::statusRadios($row[\'status\'])?>
                    </div>
                </div>
                
                <div class="form-group row mb-3">
                    <div class="col-10 offset-1">
                        <button type="submit" class="btn btn-primary smt">确认提交</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(() => {})
</script>';

        file_put_contents(\Yii::$app->basePath . '/views/adm/' . $tb->name . '_edit.html', $html);
    }
}
